package trainingJBPM.excluzard.jbpm;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Scanner;

import org.kie.api.runtime.process.ProcessInstance;
import org.kie.remote.client.api.exception.RemoteApiException;

import trainingJBPM.excluzard.jbpm.utils.JbpmUtil;

public class MainClass {

	private String processId = "";
	private Scanner mSc ;
	private boolean isMainMenu ;
	
	@SuppressWarnings("unused")
	private ProcessInstance processInstance;
	
	public static void main(String[] args) {
		MainClass mainClass = new MainClass();
		mainClass.mSc = new Scanner(System.in);
		mainClass.mainMenu();
	}
	
	private void mainMenu() {
		System.out.println("|---------------JBPM FLOW Training----------------|");
		System.out.println("|----------------------Menu-----------------------|");
		System.out.println("|------(1) set Process Id-------------------------|");
		System.out.println("|------(2) Start Flow-----------------------------|");
		System.out.println("|------(3) Next Flow By Process Instance Id-------|");
		System.out.println("|------(4) Next Flow By Process Task Id-----------|");
		System.out.println("|-------------------------------------------------|");
		isMainMenu = true;
		selectMenu("#"+this.mSc.nextLine());
	}
	
	private void selectMenu(String menuIndex) {
		
		try {
			switch (menuIndex) {
			case "m":
				mainMenu();
				break;
			case "#1":
				isMainMenu =false;
				initProcessId();
				break;
			case "#2":
				isMainMenu =false;
				processInstance = JbpmUtil.getInstant().startJBPMFlow(processId);
				mainMenu();
				break;
			case "#3":
				isMainMenu =false;
				nextFlowByProcessInstanceId();
				break;
			case "#4":
				isMainMenu =false;
				nextFlowByTaskId();
				break;
			default:
				if(isMainMenu) {
					System.out.println("|-----------------------พ่อง-----------------------|");
					mainMenu();
				}
				break;
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mainMenu();
		}catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mainMenu();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mainMenu();
		}
		
	}
	
	private void initProcessId() {
		System.out.println("|-----------------set Process Id------------------|");
		List<String> peocInstId = Config.ProcessId.getAll();
		int i=0;
		for (String iterable_element : peocInstId) {
			System.out.println("|("+(i++)+") "+iterable_element);
		}
		System.out.println("|m to MainMenu");
		System.out.println("|c to Custom");
		System.out.println("|--Enter input------------------------------------|");
		this.mSc = new Scanner(System.in);
		String input = this.mSc.nextLine();
		selectMenu(input);
		
		if(input.equalsIgnoreCase("c")) {
			System.out.println("|--Enter Custom input------------------------------------|");
			this.mSc = new Scanner(System.in);
			input = this.mSc.nextLine();
		}else {
			input = peocInstId.get(Integer.valueOf(input));
		}
		
		processId = input;
		System.out.println("|#ProcessId is "+processId+"----------------------|");
		System.out.println("|-------------------------------------------------|");
		mainMenu();
	}
	
	@SuppressWarnings("unused")
	private void nextFlowByProcessInstanceId() throws NumberFormatException, MalformedURLException {
		System.out.println("|-----------------next Flow By ProcessInstanceIdd------------------|");
		
		System.out.println("|--Enter processInstanceId ------------------------------------|");
		this.mSc = new Scanner(System.in);
		String input1 = this.mSc.nextLine();
		selectMenu(input1);
		
		System.out.println("|--Enter action ------------------------------------|");
		String action = this.mSc.nextLine();
		selectMenu(action);
		
		
		System.out.println("|#ProcessId is "+processId+"----------------------|");
		System.out.println("|-------------------------------------------------|");
		
		JbpmUtil.getInstant().nextJBPMFlowByProcessInstanceId(new Long(input1), action);
		
		nextFlowByProcessInstanceId();
		
	}
	
	@SuppressWarnings("unused")
	private void nextFlowByTaskId() throws NumberFormatException, MalformedURLException{
		System.out.println("|-----------------next Flow By nextFlowByTaskId------------------|");
		
		System.out.println("|--Enter TaskId ------------------------------------|");
		this.mSc = new Scanner(System.in);
		String input1 = this.mSc.nextLine();
		selectMenu(input1);
		
		System.out.println("|--Enter action ------------------------------------|");
		String action = this.mSc.nextLine();
		selectMenu(action);
		
		
		System.out.println("|#ProcessId is "+processId+"----------------------|");
		System.out.println("|-------------------------------------------------|");
		
		
		try {
			JbpmUtil.getInstant().nextJBPMFlowByTaskId(new Long(input1), action);
		}catch(RemoteApiException apiException ) {
			
			
			apiException.printStackTrace();
		}catch(Exception e) {
		e.printStackTrace();
		}
		
		nextFlowByTaskId();
		
	}
	
/*	private static void testQry() throws MalformedURLException {
		
		System.out.println("XXX");
		List<Status> status = new ArrayList<>();
		status.add(Status.Reserved);
//		List<TaskSummary> taskSummaries = JbpmUtil.getInstant().getTaskService().getTasksByStatusByProcessInstanceId(1, status, "en-UK");
		List<TaskSummary> taskSummaries = JbpmUtil.getInstant().getTaskService().getTasksByStatusByProcessInstanceId(1, status, null);
		System.out.println("XXX");
		
	}
	
	private static void startFlow01() throws MalformedURLException {
		ProcessInstance processInstance = JbpmUtil.getInstant().startJBPMFlow(Config.ProcessId.FLOW01);
	}
	
	private static void nexttFlow(Long processInstanceId, String action) throws MalformedURLException {
		JbpmUtil.getInstant().nextJBPMFlowByProcessInstanceId(processInstanceId, action);
	}
	
	private static void startParallelFlow() throws MalformedURLException {
		ProcessInstance processInstance = JbpmUtil.getInstant().startJBPMFlow(Config.ProcessId.PARALLEL_FLOW);
	}
	
	private static void nexttParallelFlow(Long processInstanceId, String action) throws MalformedURLException {
		JbpmUtil.getInstant().nextJBPMFlowByProcessInstanceId(processInstanceId, action);
	}
	
	private static void nexttFlowByTaskId(Long taskId, String action) throws MalformedURLException {
		JbpmUtil.getInstant().nextJBPMFlowByTaskId(taskId, action);
	}*/
	
}
