package trainingJBPM.excluzard.jbpm.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.LongSummaryStatistics;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.audit.AuditService;
import org.kie.api.runtime.manager.audit.ProcessInstanceLog;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.Task;
import org.kie.api.task.model.TaskSummary;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;


import trainingJBPM.excluzard.jbpm.Config;
import trainingJBPM.excluzard.jbpm.model.FlowData;

public class JbpmUtil {

	private static JbpmUtil jbpmUtil;
	
	public static JbpmUtil getInstant() {
		jbpmUtil = new JbpmUtil();
		return jbpmUtil;
	}
	
	private JbpmUtil() {
		super();
	}

	private RuntimeEngine setUpRuntimeEngine(){
		// สร้าง runtime egine
	      URL url = null;
		try {
			url = new URL(Config.PATN);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	      RuntimeEngine engine = RemoteRuntimeEngineFactory.newRestBuilder()
	              .addUrl(url)
	              .addUserName(Config.USER).addPassword(Config.PASSWORD)
	              .addDeploymentId(Config.DEPLOYMENT_ID)
	              .build();
	      return engine;
	  }
	  
	  public RuntimeEngine getRuntimeEngine(){
		  return setUpRuntimeEngine();
	  }
	  
	  public org.kie.api.task.TaskService getTaskService(){
		  return setUpRuntimeEngine().getTaskService();
	  }
	  
	  public KieSession getKieSession(){
		  return setUpRuntimeEngine().getKieSession();
	  }
	  
	  public AuditService getAuditService(){
		  return setUpRuntimeEngine().getAuditService();
	  }
	  
	  public ProcessInstance startJBPMFlow(String processId) throws MalformedURLException {
		  System.out.print("#Start processId: "+processId+",Task -");
		  KieSession session = getKieSession();
		  System.out.print("-");
//	      String processId = "simple001.register";
//	      String processId = "skl-los.LOSMainflow";
	      HashMap<String, Object> param = new HashMap<>();
	      ProcessInstance processInstance = session.startProcess(processId, param); //<-- START -->
	      System.out.print("-");
	      System.out.print("-> "+ processInstance.getId());
	      System.out.println("");
	      logTaskStatusReservedProcessInstanceId(processInstance.getId());
	      return processInstance;
	  }
	  
	  public void nextJBPMFlowByProcessInstanceId(Long processInstanceId, String action) throws MalformedURLException {
		  System.out.print("#Next processInstanceId: "+processInstanceId+", Action : "+action);
//		  List<Long> jbpmTaskIds = taskService.getTasksByProcessInstanceId(processInstanceId);
		  List<TaskSummary> jbpmTaskIds = getTaskStatusReservedByProcessInstanceId(processInstanceId);
		  if(jbpmTaskIds!=null && jbpmTaskIds.size()>0){
			  for(TaskSummary taskSummary : jbpmTaskIds) {
				  String log = String.format("#Next Task [ ID=%s, Name=%s, Status=%s ]", taskSummary.getId(), taskSummary.getName(), taskSummary.getStatus());
				  System.out.println(log);
				  nextJBPMFlowByTaskId(taskSummary.getId(), action);
			  }
		  }
	  }
	  
	  public void nextJBPMFlowByTaskId(Long taskId, String action) throws MalformedURLException {
		  TaskService taskService = getTaskService();
		  HashMap<String, Object> param = new HashMap<>();
		  
		  Task task = taskService.getTaskById(taskId);
		  Long processInstanceId = null;
		  Long jbpmTaskId = null;
		  
		  if(task!=null){
			  
			    FlowData outFlowData = new FlowData();
			    outFlowData.setAction(action==null?"approve":action);
//			    outFlowData.setUser(Config.USER);
//		        param.put("in_flowData", outFlowData);
		        
		        jbpmTaskId = task.getId();
		        processInstanceId = task.getTaskData().getProcessInstanceId();
		        
		        System.out.println("");
		        if(task.getTaskData().getStatus().equals(Status.Ready)) {
		        	taskService.claim(jbpmTaskId, Config.USER);
		        	System.out.print("Cliam this task");
		        }
		        if(task.getTaskData().getStatus().equals(Status.Reserved) || task.getTaskData().getStatus().equals(Status.Ready)) {
		        	taskService.start(taskId, Config.USER);
		        	System.out.print("-->Start this task");
		        }
		        taskService.complete(jbpmTaskId, Config.USER, param);
		        System.out.print("-->complete this task");
		        
		        logTaskStatusReservedProcessInstanceId(processInstanceId);
		  }
	  }
	  
	  public String nextJBPMFlow(Long processInstanceId, String action) throws MalformedURLException {
		  TaskService taskService = getTaskService();
//		  HashMap<String, Object> param = new HashMap<>();
		  List<Long> jbpmTaskIds = taskService.getTasksByProcessInstanceId(processInstanceId);
//		  taskService.get
//		  getKieSession().getQueryResults(query, arguments)
		  if(jbpmTaskIds!=null && jbpmTaskIds.size()>0){
////			  Long jbpmTaskId = jbpmTaskIds.get(jbpmTaskIds.size()-1);
			  Long jbpmTaskId = getTaskIdMax(jbpmTaskIds);
//			  System.out.println("jbpmTaskId : "+jbpmTaskId);
//			  taskService.start(jbpmTaskId , username);
////		        param.put("out_listReq", listFlowData);
//			    FlowData outFlowData = new FlowData();
////			    outFlowData.setAction("approve");
//			    outFlowData.setAction(action==null?"approve":action);
//			    outFlowData.setUser(username);
//		        param.put("out_flowData", outFlowData);
//		        taskService.complete(jbpmTaskId, username, param);
////		        genConsoleLog(taskService, processInstanceId, jbpmTaskId);
			  
//              // Claim Task
//              taskService.claim(taskSummary.getId(), "mary");
//              // Start Task
//              taskService.start(taskSummary.getId(), "mary");
			  
//		        
		  }
////		  return allLog.toString();
		  return "";
	  }
	  
	  private long getTaskIdMax(List<Long> jbpmTaskIds){
		  /*array version*/
//	      LongSummaryStatistics stat = Arrays.stream(jbpmTaskIds).mapToLong((x)-> x.longValue()).summaryStatistics();
		  /*Int Array version*/
//		  int[] ss = {1};
//		  IntSummaryStatistics stats = Arrays.stream(ss).summaryStatistics();
		  
		  LongSummaryStatistics stat = jbpmTaskIds.stream().mapToLong((x)->x.longValue()).summaryStatistics();
		  return stat.getMax();
	  }
	  
	  public List<TaskSummary> getTaskStatusReservedByProcessInstanceId(Long ProcessInstanceId){
			List<Status> status = new ArrayList<>();
			status.add(Status.Reserved);
			status.add(Status.Ready);
			status.add(Status.InProgress);
//			List<TaskSummary> taskSummaries = JbpmUtil.getInstant().getTaskService().getTasksByStatusByProcessInstanceId(1, status, "en-UK");
			List<TaskSummary> taskSummaries = JbpmUtil.getInstant().getTaskService().getTasksByStatusByProcessInstanceId(ProcessInstanceId, status, null);
			return taskSummaries;
	  }
	  
	  public void logTaskStatusReservedProcessInstanceId(Long processInstanceId){
		  List<TaskSummary> taskSummaries = getTaskStatusReservedByProcessInstanceId(processInstanceId);
		  System.out.println();
		  if(taskSummaries==null || taskSummaries.size()==0){
			  
			  /*findSubProcess nonRecursive*/
//			  taskSummaries = new ArrayList<>();
//			  List<? extends ProcessInstanceLog> instanceLogs  = JbpmUtil.getInstant().getAuditService().findSubProcessInstances(processInstanceId);
//			  if(instanceLogs!=null && instanceLogs.size()>0) {
//				  System.out.println("#subProceseInstance of ProcessInstanceId : "+processInstanceId);
//				  for(int i=0 ; i<instanceLogs.size();i++) {
//					  System.out.println("#subProceseInstance("+(i+1)+") : "+instanceLogs.get(i).getProcessInstanceId());
//					  taskSummaries.addAll(getTaskStatusReservedByProcessInstanceId(instanceLogs.get(i).getProcessInstanceId()));
//				  }
//			  }else {
//				  System.out.println("#End of ProcessInstanceId : "+processInstanceId);
//				  return;
//			  }
			  
			  /*findSubProcess recursive*/
			  List<? extends ProcessInstanceLog> instanceLogs  = JbpmUtil.getInstant().getAuditService().findSubProcessInstances(processInstanceId);
			  if(instanceLogs!=null && instanceLogs.size()>0) {
				  System.out.println("#subProceseInstance of ProcessInstanceId : "+processInstanceId);
				  for(int i=0 ; i<instanceLogs.size();i++) {
					  System.out.println("#subProceseInstance("+(i+1)+") : "+instanceLogs.get(i).getProcessInstanceId());
					  logTaskStatusReservedProcessInstanceId(instanceLogs.get(i).getProcessInstanceId());
				  }
			  }else {
				  System.out.println("#End of ProcessInstanceId : "+processInstanceId);
				  return;
			  }
			  
		  }
		  
		  for(TaskSummary taskSummary : taskSummaries) {
			  String log = String.format("#Curent task [ProcessInstanceId=%s, Task_ID=%s, Task_Name=%s, Status=%s ]", processInstanceId, taskSummary.getId(), taskSummary.getName(), taskSummary.getStatus());
			  System.out.println(log);
		  }
		  
	  }
	  
//	  private void genConsoleLog(TaskService taskService, Long processInstanceId, Long oldTaskId){
//		  if(taskService==null){
//			  taskService = getTaskService();
//		  }
////		  StringBuilder allLog = new StringBuilder();
//		  List<Long> jbpmTaskIds = taskService.getTasksByProcessInstanceId(processInstanceId);
//		  Long nextJbpmTaskId = getTaskIdMax(jbpmTaskIds);
//		  if(oldTaskId.equals(nextJbpmTaskId)){
//			  System.out.println("complete task id : "+oldTaskId+" in processInstanceId : "+processInstanceId);
//			  allLog.append("complete task id : "+oldTaskId+" in processInstanceId : "+processInstanceId);
//			  allLog.append(SP);
//		  }else{
//			  System.out.println(oldTaskId+" to "+ nextJbpmTaskId);
//			  allLog.append(oldTaskId+" to "+ nextJbpmTaskId);
//			  allLog.append(SP);
//			  Task task = taskService.getTaskById(nextJbpmTaskId);
//			  System.out.println("curent task Name : "+task.getName());
//			  allLog.append("curent task Name : "+task.getName());
//			  allLog.append(SP);
//		  }
////		  return allLog.toString();
//	  }
	  
}
