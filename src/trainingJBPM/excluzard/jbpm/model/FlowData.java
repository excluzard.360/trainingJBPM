package trainingJBPM.excluzard.jbpm.model;

public class FlowData {
	private String action;
	private String user;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	@Override
	public String toString() {
		return String.format("FlowData [action=%s, user=%s]", action, user);
	}
	
	
}
