trainingJBPM 
==============================

Main Feature
===========================
* set processId.
* start flow.
* next flow by processInstancId.
* next flow by taskId.

Config
===========================
trainingJBPM.excluzard.jbpm.Config

	public static final String PATN = "http://xx.xx.xx.xxx:8080/jbpm-console";
	public static final String DEPLOYMENT_ID = "com.x.ccorg:trainFlow:0.0.1";
	public static final String USER = "<user jbpm>";
	public static final String PASSWORD = "<password jbpm>";
	
	class ProcessId{
	    .
	    .
	    .
	    
	    private static final String PROJECT_NAME = "<projectName>";
	    
	    public static final String FLOW01 = PROJECT_NAME+"."<processName or flowName>";
	    .
	    .
	    .
	}


 ![Header image](https://gitlab.com/excluzard.360/trainingJBPM/raw/9f91a8f2c32bd582379123f71b1c3bcd0cfe2904/img/001.png)

